﻿1
00:00:00,540 --> 00:00:01,640
   ﷽ In the name of Allah

2
00:00:01,640 --> 00:00:02,700
Qas Al-Hak Book

3
00:00:02,800 --> 00:00:07,290
Is a book that is trying to explain to communities

4
00:00:07,300 --> 00:00:10,330
The right of individuals which leads them to

5
00:00:10,550 --> 00:00:11,950
Achieve dignity

6
00:00:11,960 --> 00:00:14,990
What is going now, with good intentions

7
00:00:15,000 --> 00:00:19,670
That some scholars are focusing on issues

8
00:00:19,820 --> 00:00:22,920
That may lead people away from their rights

9
00:00:24,000 --> 00:00:26,280
For example they focus on prayers and fasting

10
00:00:26,290 --> 00:00:29,250
And it's important, to focus on practices is important

11
00:00:29,260 --> 00:00:34,730
But that doesn't mean to not focus on rights

12
00:00:35,640 --> 00:00:39,060
Like the right of people to possess minerals

13
00:00:39,250 --> 00:00:41,690
People's right

14
00:00:41,880 --> 00:00:46,590
To build factories without permissions from govts.

15
00:00:46,590 --> 00:00:47,970
And the like

16
00:00:47,980 --> 00:00:50,970
Sure that may surprise someone

17
00:00:50,980 --> 00:00:54,420
On how to create factories without govts permissions

18
00:00:54,420 --> 00:00:57,960
"That will lead to chaos and env. pollution"

19
00:00:59,550 --> 00:01:02,840
Sharia sliced rights

20
00:01:02,850 --> 00:01:04,950
I seek refuge in Allah from the accursed devil

21
00:01:04,950 --> 00:01:09,190
إن الحكم إلا لله يقص الحق وهو خير الفاصلين
The decision is only for Allāh. He relates the truth, and He is the best of deciders

22
00:01:09,200 --> 00:01:12,930
It's like when you go to the tailor, they tailor your dress

23
00:01:12,930 --> 00:01:14,650
He take your measurements and tailors it

24
00:01:14,660 --> 00:01:18,950
God the Almighty tailored the rights for human kinds

25
00:01:18,990 --> 00:01:22,680
If we don't follow we will pollute earth

26
00:01:22,680 --> 00:01:24,540
And that what is happening in the West

27
00:01:24,550 --> 00:01:27,890
So Qas Al-Hak message is not only directed to Muslims

28
00:01:27,890 --> 00:01:30,300
But also for non-Muslims

29
00:01:30,300 --> 00:01:32,580
Watch out you will pollute earth

30
00:01:32,580 --> 00:01:36,460
Sure, we can't have argue with non-Muslim communities

31
00:01:36,470 --> 00:01:39,110
About issues like adultery

32
00:01:39,120 --> 00:01:41,030
Alcohol, and similar, those are value issues

33
00:01:41,040 --> 00:01:44,180
But there are issues that we have to agree on

34
00:01:44,190 --> 00:01:48,100
Like opening the doors of empowerment

35
00:01:48,230 --> 00:01:50,840
Which ends unemployment

36
00:01:50,840 --> 00:01:52,960
And ends monopoly

37
00:01:53,080 --> 00:01:56,440
And leads to rationalization of consumption

38
00:01:56,550 --> 00:01:58,560
Of earth resources without pollution

39
00:01:58,720 --> 00:01:59,980
It's a long subject

40
00:01:59,980 --> 00:02:01,830
But the simple idea is

41
00:02:02,450 --> 00:02:04,260
Focus was

42
00:02:06,360 --> 00:02:07,590
In recent period

43
00:02:07,590 --> 00:02:10,440
- Early scholars didn't need it -

44
00:02:10,440 --> 00:02:12,480
Focus was on practices

45
00:02:12,480 --> 00:02:15,930
Which is important without eliminating the focus

46
00:02:16,680 --> 00:02:18,320
On people's rights

47
00:02:18,320 --> 00:02:22,470
For example some countries are rich with resources

48
00:02:22,470 --> 00:02:25,740
They take it and spend it on stuff like

49
00:02:25,740 --> 00:02:29,090
Football stadiums or opera houses, but this is forbidden in Islam

50
00:02:29,090 --> 00:02:32,250
Why? Because if it was Fai' money

51
00:02:33,120 --> 00:02:37,110
It belongs to people the verse of Fai' have specified

52
00:02:37,110 --> 00:02:39,390
I seek refuge with Allah from the accursed Satan

53
00:02:40,590 --> 00:02:42,960
ما أفاء الله على رسوله من أهل القرى
Whatever fai' Allah has passed on to His Messenger from the people of the towns

54
00:02:43,500 --> 00:02:44,580
It's specific categories

55
00:02:45,990 --> 00:02:50,270
Plus, if the govt. didn't take it, it's up to people to take

56
00:02:50,850 --> 00:02:54,770
A group of young Syrians or Turks may extract

57
00:02:54,770 --> 00:02:56,970
These metals, it belongs to them

58
00:02:56,970 --> 00:02:59,360
Everyone says this will lead to chaos, will lead to monopoly

59
00:02:59,570 --> 00:03:01,620
Qas Al-Hak says: No, It doesn't. It leads to

60
00:03:02,490 --> 00:03:04,430
Better outcomes to humanity

61
00:03:04,970 --> 00:03:06,800
Reporter: One last question Dr.

62
00:03:07,600 --> 00:03:12,480
Is there a relation between rights and law in the book

63
00:03:12,480 --> 00:03:15,690
Is there a legal reference or what do you mean by rights (Haq) 

64
00:03:16,890 --> 00:03:20,250
Relation between rights and law is

65
00:03:20,250 --> 00:03:22,340
Any law you look at

66
00:03:22,350 --> 00:03:25,140
Is in reality rights, for example if you say

67
00:03:25,150 --> 00:03:28,310
People on this street can have their building up to

68
00:03:28,350 --> 00:03:30,560
Four to five floors

69
00:03:30,570 --> 00:03:32,040
If it was two and become five

70
00:03:32,050 --> 00:03:33,870
You increased people rights

71
00:03:33,870 --> 00:03:35,590
Increased their wealth without their effort

72
00:03:35,770 --> 00:03:39,060
So you always think and find that laws

73
00:03:39,060 --> 00:03:40,640
Become rights

74
00:03:40,720 --> 00:03:41,920
Now if it become rights

75
00:03:41,930 --> 00:03:43,580
You have three possibilities

76
00:03:43,590 --> 00:03:44,940
Either those rights

77
00:03:44,940 --> 00:03:48,330
We created by law exists in Sharia

78
00:03:48,340 --> 00:03:50,270
Then we relay on Sharia

79
00:03:50,270 --> 00:03:52,060
Or it's against Sharia, we leave it

80
00:03:52,060 --> 00:03:54,710
Or it's something missing that Sharia didn't have

81
00:03:54,720 --> 00:03:56,660
So laws comes to complete it

82
00:03:56,660 --> 00:03:58,650
Here, Qas Al-Haq book

83
00:03:58,890 --> 00:04:01,870
Challenges people to give one issue

84
00:04:01,880 --> 00:04:03,410
- In matters of development and economy -

85
00:04:03,490 --> 00:04:05,000
I don't talk about other issues

86
00:04:05,000 --> 00:04:07,070
Only issues of development, economy and urbanization

87
00:04:07,080 --> 00:04:08,160
That Sharia didn't include

88
00:04:08,160 --> 00:04:11,170
Sharia as have been revealed to the prophet ﷺ

89
00:04:11,180 --> 00:04:14,790
Is complete and suitable for our days

90
00:04:15,030 --> 00:04:19,010
Qas Al-Haq book argues that it's valid for our days

91
00:04:19,020 --> 00:04:22,480
And it becomes more valid as we proceed in future

92
00:04:22,480 --> 00:04:24,710
I mean, Sharia is more suitable today than yesterday

93
00:04:24,720 --> 00:04:26,510
And tomorrow more than today

94
00:04:26,780 --> 00:04:29,580
Sure this isn't an acceptable view

95
00:04:29,640 --> 00:04:31,450
Many people refuses it

96
00:04:31,780 --> 00:04:34,470
Qas Al-Haq argues in it's favor

